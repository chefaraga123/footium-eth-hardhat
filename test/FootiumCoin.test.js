const { expect } = require("chai");
const { ethers } = require('hardhat');

async function deploy(name, ...params) {
    const Contract = await ethers.getContractFactory(name);
    return await Contract.deploy(...params).then(f => f.deployed());
}

describe("FootiumCoin", () => {
    before(async () => {

        [owner, addr1, addr2, _] = await ethers.getSigners();      

        //deploy the club contract
        CoinContract = await deploy(
            'FootiumCoin', 
            50000000
        )
    })

    describe("token attributes", () => {
        it("Has the correct name", async () => {
            const name = await CoinContract.name();

            expect(name).to.equal("FootiumToken");
        });

        it("Has the correct symbol", async () => {
            const symbol = await CoinContract.symbol();

            expect(symbol).to.equal("Foot");
        });
    });

    describe("initalSupply", () => {
        it("Has initial supply of 3000", async () => {
            const initialSupply = await CoinContract.initialSupply();

            expect(initialSupply.toNumber()).to.equal(50000000);
        });

        it("Account 0 holds the initial supply", async () => {
            const initialSupply = await CoinContract.initialSupply();
            const account0Balance = await CoinContract.balanceOf(
                owner.address
            );

            expect(account0Balance.toNumber()).to.equal(
                initialSupply.toNumber()
            );
        });
    })

    describe(".transfer()", async () => {
        it("Account 0 transfers 500FMC to account 1", async () => {
            await CoinContract.transfer(
                addr1.address, 
                500
            );

            const account1Balance = await CoinContract.balanceOf(
                addr1.address
            );
            const account0Balance = await CoinContract.balanceOf(
                owner.address
            );

            expect(account1Balance.toNumber()).to.equal(500);
            expect(account0Balance.toNumber()).to.equal(49999500);
        });

        it("Account 1 transfers 100FMC to account 2", async () => {
            await CoinContract.connect(addr1).transfer(addr2.address, 100);

            const account2Balance = await CoinContract.balanceOf(
                addr2.address
            );

            expect(account2Balance.toNumber()).to.equal(100);
        });
    });

    

});