const { expect } = require("chai");
const { ethers } = require('hardhat');

const { merkleTree, merkleRoot } = require("../scripts/merkle.js");
const whitelist = require('../scripts/testWhitelist.json');

const { constants } = require('@openzeppelin/test-helpers');  

//const FootballClub = artifacts.require("FootballClub");
 
async function deploy(name, ...params) {
    const Contract = await ethers.getContractFactory(name);
    return await Contract.deploy(...params).then(f => f.deployed());
}

let ClubContract, owner, addr1, addr2 

function hashToken(account) {
    return Buffer.from(ethers.utils.solidityKeccak256(['address'], [account]).slice(2), 'hex')
  }
  

describe("FootballClub", () => {

    before(async () => {
        //dont get address directly, you get a signer object
        [owner, addr1, addr2, _] = await ethers.getSigners();
    
        ClubContract = await deploy(
            'FootballClub', 
            "http://localhost:3001/api",
            merkleRoot, 
            50000000
        )
    })

    describe("Deployment", () => {
        it("should be the right owner", async () => {
            expect(await ClubContract.owner()).to.equal(owner.address)
        })
    })


    describe(".redeem()", () => {

        context("proof made with an invalid address", () => {
          it("fails to mint", async () => {
    
            const account = await addr1.getAddress()
    
            const token = hashToken(account)
            
            const proof = merkleTree.getHexProof(token);
            
            await expect(ClubContract.redeem(account, proof, 
              { 
                value: ethers.utils.parseEther("0.05")          
              })).to.be.revertedWith(
                "Invalid merkle proof"
              );
          })
        })
    
        context("proof made with a valid address", () => {
          describe('Test all the addresses in the whitelist', function () {
            context("the accounts send enough ether", () => {
              for (let i = 0; i< whitelist.addresses.length; i++) {
                it('mints an NFT', async function () {
                  /**
                   * Create merkle proof (anyone with knowledge of the merkle tree)
                   */
                  const account = whitelist.addresses[i]
                  const token = hashToken(account)
                  const proof = merkleTree.getHexProof(token);

                  //const totalSupply = await ClubContract.totalSupply(); 
                  //console.log('totalSupply',totalSupply.toNumber())
                  //expect(
                  //  totalSupply
                  //  ).to.equal(i)
                  
          
                  /**
                   * Redeems token using merkle proof (anyone with the proof)
                   */
                  await expect(
                    ClubContract.redeem(
                      account, 
                      proof, 
                    { 
                      value: ethers.utils.parseEther("0.05")          
                    }))
                    .to.emit(
                      ClubContract, 
                      'Transfer'
                    ).withArgs(
                      constants.ZERO_ADDRESS, 
                      account, 
                      i+1
                    )
                  

                });
              }
            })
          });
        
          describe('Test all the addresses in the whitelist', function () {
            context("the accounts send too little ether", () => {
              for (let i = 0; i< whitelist.addresses.length; i++) {
                it('fails to mint', async function () {
                  /**
                   * Create merkle proof (anyone with knowledge of the merkle tree)
                   */
                  const account = whitelist.addresses[i]
                  const token = hashToken(account)
                  const proof = merkleTree.getHexProof(token);
          
                  /**
                   * Redeems token using merkle proof (anyone with the proof)
                   */
          
                  await expect(ClubContract.redeem(account, proof, 
                    { 
                      value: ethers.utils.parseEther("0.04")          
                    })).to.be.revertedWith(
                      "insufficient payment"
                    )
                });
              }
            })
          });


        })
    });
});