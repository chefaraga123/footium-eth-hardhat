const { expect } = require("chai");
const { ethers } = require('hardhat');

const { merkleRoot } = require("../scripts/merkle.js");

const {
    BN,           // Big Number support
    constants,    // Common constants, like the zero address and largest integers
    expectEvent,  // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
  } = require('@openzeppelin/test-helpers');  

async function deploy(name, ...params) {
    const Contract = await ethers.getContractFactory(name);
    return await Contract.deploy(...params).then(f => f.deployed());
}

describe("PlayerStaking", () => {
    
    before(async () => {

        [owner, addr1, addr2, addr3, _] = await ethers.getSigners();      

        ClubContract = await deploy(
            "FootballClub", 
            "http://localhost:3001/api",
            merkleRoot, 
            50000000
        )

        PlayerContract = await deploy(
            'FootballPlayer', 
            ClubContract.address,
            "http://localhost:3001/api"
        )


        StakingContract = await deploy(
            'PlayerStaking', 
            PlayerContract.address,
            ClubContract.address
        )
        

        //issue two clubs to address1 
        await ClubContract.preMint(owner.address);

        await PlayerContract.mintPlayer(0, 2000, 0, 0);
        await PlayerContract.mintPlayer(0, 2000, 1, 0);
        await PlayerContract.mintPlayer(0, 2000, 2, 0);
        

        await ClubContract.preMint(addr1.address);
        
        await PlayerContract.connect(addr1).mintPlayer(1, 2000, 0, 1);
        await PlayerContract.connect(addr1).mintPlayer(1, 2000, 1, 1);
        await PlayerContract.connect(addr1).mintPlayer(1, 2000, 2, 1);

        await PlayerContract.setStakingAddress(StakingContract.address);
    });

    describe("initial setup", () => {
        it("has minted enough players", async () => {
            const a = await PlayerContract.getSignedPlayersByClub(0)
            const b = await PlayerContract.getSignedPlayersByClub(1)
    
            for(let i = 0; i < 3; i++){
                expect(a[i].toNumber()).to.equal(i)
                expect(b[i].toNumber()).to.equal(i+3)
            }

        })
    })

    context("there are no staked players", () => {

        describe(".stakePlayer()", () => {
            it("club 0 stakes player Id=0", async () => {

                await StakingContract.stakePlayer(0)

            })

            it("club 1 stakes player Id=3", async () => {

                await StakingContract.stakePlayer(3)

            })
        })


    })

});
