const { expect } = require("chai");
const { ethers } = require('hardhat');
const { merkleRoot } = require("../scripts/merkle.js");

async function deploy(name, ...params) {
    const Contract = await ethers.getContractFactory(name);
    return await Contract.deploy(...params).then(f => f.deployed());
}

describe("FootiumCoinStaking", () => {

    before(async () => {

        [owner, addr1, addr2, addr3, _] = await ethers.getSigners();      

        //deploy the coin contract
        CoinContract = await deploy(
            'FootiumCoin', 
            50000000
        )

        //deploy the club contract
        StakingContract = await deploy(
            'FootiumCoinStaking', 
            CoinContract.address,
            1209600,
            5
        )

    })

    describe("attributes", () => {
        it("sets the coin contract address correctly", async () => {
          const tokenAddress = await StakingContract.tokenAddress();  
          expect(tokenAddress).to.equal(CoinContract.address)
        })
    })

    
    describe(".createStake()", () => {
        context("address 0's staking into club 1", () => {
            it("Account 0 approves an allowance of 300 tokens", async () => {

                await CoinContract.approve(
                    StakingContract.address,
                    300,
                );
                
                const amount = await CoinContract.allowance(
                    owner.address,
                    StakingContract.address
                )

                expect(amount.toNumber()).to.equal(300)
            });

            it("Account 0 transfers 300FMC to the staking contract", async () => {

                expect(await StakingContract.createStake(
                    30, 
                    1
                )).to.emit(CoinContract, 'Transfer');

                const ownerBalance = await CoinContract.balanceOf(
                    owner.address
                );

                expect(ownerBalance.toNumber()).to.equal(49999970);
            });

            it("There are 300 FMC in the staking contract", async () => {
                const amountStaked =
                    await StakingContract.clubBalances(1);

                expect(amountStaked.toNumber()).to.equal(30);
            });

            it("Club 1's stake status is true", async () => {
                depositTime = await StakingContract.depositTime(1);

                const club1StakeStatus =
                    await StakingContract.clubStakeStatus(1);

                expect(club1StakeStatus).to.be.true;
            });
        });
    });

    describe(".offerClubForSale()", () => {

        let ClubContract;

        before(async () => {
            //transfer tokens to one of the accounts
            await CoinContract.transfer(
                addr2.address, 
                500
            );

            //deploy an instance of the club contracts
            ClubContract = await deploy(
                'FootballClub', 
                "http://localhost:3001/api",
                merkleRoot, 
                50000000
            )


            await CoinContract.connect(addr2).approve(
                StakingContract.address,
                300                
            );

            await ClubContract.setStakingAddress(StakingContract.address);

            await ClubContract.preMint(addr2.address);
            await ClubContract.preMint(addr2.address);
            await ClubContract.preMint(addr2.address);
            await ClubContract.preMint(addr2.address);
        })

        context("club 3 is being sold whilst it has a stake", () => {

            it("properly creates a stake in the club", async () => {
                expect(
                    await StakingContract.connect(
                    addr2
                    ).createStake(
                    300, 
                    3
                )).to.emit(
                    StakingContract,
                    'tokensStaked'
                ).withArgs(
                    3,
                    300
                );
            })

            it("Correct Staking Address in the Club Contract", async () => {
                expect(
                    await ClubContract.stakingAddress()
                ).to.equal(
                    StakingContract.address
                );
            });


            it("account 2 puts club 3 up for sale for 200 wei", async () => {

                expect((
                    await ClubContract.clubsOfferedForSale(3)
                    ).isForSale
                ).to.be.false;

                expect(await ClubContract.connect(
                    addr2
                ).offerClubForSale(
                    3, 
                    200
                )).to.emit(
                    ClubContract,
                    'ClubOffered'
                ).withArgs(
                    3, 
                    200
                );

                expect((
                    await ClubContract.clubsOfferedForSale(3)
                    ).isForSale
                ).to.be.true;

            });

            it("Account 3 buys club 3", async () => {
                await ClubContract.connect(
                    addr2
                ).approve(
                    addr3.address,
                    3
                )

                await ClubContract.connect(
                    addr3
                ).buyClub(3, {
                    value: 200
                });

                expect(
                    await ClubContract.ownerOf(3)
                ).to.equal(
                    addr3.address
                );
            });

            it("Stake has been removed from the staking contract", async () => {
                expect(
                    (
                        await StakingContract.clubBalances(
                    3
                )
                ).toNumber()).to.equal(0);
            });

            it("Stake has been returned to address 2 less a fee ", async () => {

                expect(
                    (
                        await CoinContract.balanceOf(
                    addr2.address
                    )
                ).toNumber()).to.equal(485);

            });
        });
    });

    describe(".withdrawStake()", () => {
        context("there is a stake in club 3", () => {

            before(async () => {
                
            })

            it("The investment is withdrawn", async () => {
                await StakingContract.withdrawStake(
                    200,
                    2,
                );

                const balance = await StakingContract.clubBalances(
                    2
                );

                expect(
                    balance.toNumber()
                ).to.equal(0);
            });

            it("A fee has been incurred due to the premature withdrawal", async () => {
                const balance = await CoinContract.balanceOf(
                    accounts[1]
                );

                expect(balance.toNumber()).to.equal(490);
            });

            it("The stake status has been set to false", async () => {
                club1Stakestatus =
                    await StakingContract.clubStakeStatus(2);

                expect(club1Stakestatus).to.equal(false);
            });
        });

        context(
            "Account 0 withdraws their stake after the minimum duration",
            () => {
                it("The time is set a week in the future", async () => {
                    const timeBefore =
                        await StakingContract.getCurrentTime();

                    const week = 604800;

                    await timeHelper.timeTravel(week + 1);

                    const timeAfter =
                        await StakingContract.getCurrentTime();

                    expect(timeAfter.toNumber()).above(
                        timeBefore.toNumber() + week - 10
                    );
                    expect(timeAfter.toNumber()).below(
                        timeBefore.toNumber() + week + 10
                    );
                });

                it("Account 0 withdraws its stake without paying a penalty ", async () => {
                    await StakingContract.withdrawStake(
                        300,
                        1,
                        accounts[0]
                    );

                    const account0balance2 =
                        await CoinContract.balanceOf(accounts[0]);

                    expect(account0balance2.toNumber()).to.equal(500);
                });

                it("The stake status has been set to false", async () => {
                    club1Stakestatus =
                        await StakingContract.clubStakeStatus(1);

                    expect(club1Stakestatus).to.equal(false);
                });
            }
        );
    });
});
