const { expect } = require("chai");
const { ethers } = require('hardhat');

const {
    BN,           // Big Number support
    constants,    // Common constants, like the zero address and largest integers
    expectEvent,  // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
  } = require('@openzeppelin/test-helpers');  

const { merkleRoot } = require("../scripts/merkle.js");

async function deploy(name, ...params) {
    const Contract = await ethers.getContractFactory(name);
    return await Contract.deploy(...params).then(f => f.deployed());
}

let PlayerContract, ClubContract, owner, addr1, addr2 


describe("FootballPlayer", () => {

    before(async () => {

        [owner, addr1, addr2, addr3, _] = await ethers.getSigners();      

        //deploy the club contract
        ClubContract = await deploy(
            'FootballClub', 
            "http://localhost:3001/api",
            merkleRoot, 
            50000000
        )

        //pass the address of the club contract into the constructor
        PlayerContract = await deploy(
            'FootballPlayer', 
            ClubContract.address,
            "http://localhost:3001/api"
        )

        await ClubContract.preMint(addr1.address);
        await ClubContract.preMint(addr2.address);
        await ClubContract.preMint(addr3.address);

    })

    describe("Deployment", () => {
        it("should be the right owner", async () => {
            expect(await PlayerContract.owner()).to.equal(owner.address)
        })
        
        it("clubs should have been transferred", async () => {
            expect(await ClubContract.balanceOf(addr1.address)).to.equal(1)
            expect(await ClubContract.balanceOf(addr2.address)).to.equal(1)
            expect(await ClubContract.balanceOf(addr3.address)).to.equal(1)

        })
    })
    
    describe(".mintPlayer()", () => {
        it("mints a player to club 1", async () => {
            expect(await PlayerContract.connect(
                addr1
                ).mintPlayer(
                1, 2000, 1, 1
            )).to.emit(
                PlayerContract, 
                'Transfer'
            ).withArgs(
                constants.ZERO_ADDRESS, 
                addr1.address, 
                1
            );
 
            //expect(await PlayerContract.balanceOfInternal(0)).to.equal(1)
        })

        it("mints a player to club 2", async () => {
            expect(await PlayerContract.connect(
                addr2
                ).mintPlayer(
                2, 2000, 1, 2
            )).to.emit(
                PlayerContract, 
                'Transfer'
            ).withArgs(
                constants.ZERO_ADDRESS, 
                addr2.address, 
                2
            );

            expect(await PlayerContract.balanceOfInternal(1)).to.equal(1)

            const addr2Bal = await PlayerContract.balanceOf(addr2.address)

            expect(addr2Bal.toNumber()).to.equal(1)

        })
    });
    /*
    describe(".mintBatchPlayers()", () => {
        it("mints to club 1",async () => {
            await PlayerContract.connect(addr2).mintBatchPlayers(
                [
                    [1, 2000, 0, 1], // repition so should be excluded
                    [1, 1998, 1, 1]
                ]
            );

            //expect(await PlayerContract.balanceOfInternal(1)).to.equal(2)
        })
    })*/

    describe(".mintAsIndividual()", () => {
        it("mints to addr1", async () => {
            expect(await PlayerContract.connect(
                addr1
            ).mintAsIndividual(
                1, 1998, 1
            )).to.emit(
                PlayerContract, 
                'Transfer'
            ).withArgs(
                constants.ZERO_ADDRESS, 
                addr1.address, 
                3
            );
                
            expect(await PlayerContract.balanceOf(addr1.address)).to.equal(2)
        })
    })

    describe("getSignedPlayersByClub", () => {
        it("returns the correct number of players", async () => {
            const signedPlayers = await PlayerContract.getSignedPlayersByClub(1); 
            expect(signedPlayers.length).to.equal(1)
        })
    })

    describe("OfferForSale and WithdrawFromSale", () => {

        it("player is mapped correctly", async () => {
            const ownerClub = await PlayerContract.playerToClub(1)
            expect(ownerClub.toNumber()).to.equal(1)
        })

        it("club 1 puts player 1 up for sale", async () => {
            expect(await PlayerContract.connect(
                addr1
            ).offerPlayerForSale(
                1,
                1, 
                100
            )).to.emit(
                PlayerContract,
                'PlayerPutUpForSale'
            ).withArgs(
                1,
                100
            )

            /*expect(await PlayerContract.checkPlayerSaleStatus(
                1
            )).to.be.true;*/

        });

        it("club 1 withdraws player 1 from sale", async () => {
            expect(await PlayerContract.connect(
                addr1
            ).withdrawPlayerFromSale(
                1,
                1
            )).to.emit(
                PlayerContract,
                'PlayerOfferWithdrawn'
            ).withArgs(
                1
            );

            /*expect(await PlayerContract.checkPlayerSaleStatus(
                1
            )).to.be.false;*/
        });
    });

    describe("OfferForSale and Buy", () => {

        it("club 1 puts player 1 up for sale", async () => {
            expect(await PlayerContract.connect(
                addr1
            ).offerPlayerForSale(
                1,
                1,
                100
            )).to.emit(
                PlayerContract,
                'PlayerPutUpForSale'
            ).withArgs(
                1, 
                100
            );

            expect(await PlayerContract.checkPlayerSaleStatus(
                1
            )).to.be.true;

            /*const salePrice = await PlayerContract.checkPlayerSalePrice(
                1
            );
            expect(salePrice.toNumber()).to.equal(100);*/
        });

        it("club 1 buys player 2", async () => {
            expect(await PlayerContract.connect(
                addr1
            ).buyPlayer(
                2, 1, 
                { value: ethers.utils.parseEther("50") }
            )).to.emit(
                PlayerContract,
                'PlayerBought'
            ).withArgs(
                2,
                ethers.utils.parseEther("50"),
                2,
                1
            );
            
            const newOwner = await PlayerContract.ownerOfUint(2);
            expect(newOwner.toNumber()).to.equal(1);
        });
    });

    describe(".tokenURI", () => {
        context("given player with ID 1", () => {
            it("returns the correct token URI", async () => {

                const playerArray = await PlayerContract.players(0);

                console.log(playerArray[0].toNumber()) //academy club index
                console.log(playerArray[1].toNumber()) //birthYear
                console.log(playerArray[2].toNumber()) //generationId 


                //const a = await PlayerContract.testOwner(1)
                //console.log('owner club',a.toNumber())


                const b = await PlayerContract.getSignedPlayersByClub(1)
                console.log(b)
                const c = await PlayerContract.getSignedPlayersByClub(2)
                console.log(c)

                const uri = await PlayerContract.tokenURI(1);

                //expect(uri).to.equal(
                //    config.api + "/nfts/players/2000-1-1/metadata"
                //);
            });
        });
    });
});

//1, 2000, 0, 1