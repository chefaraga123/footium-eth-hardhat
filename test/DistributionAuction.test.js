const timeHelper = require("../scripts/timeHelper");

const { 
    assert, 
    expect 
} = require("chai");

const { ethers } = require('hardhat');

const { merkleTree, merkleRoot } = require("../scripts/merkle.js");

const {
    BN,           // Big Number support
    constants,    // Common constants, like the zero address and largest integers
    expectEvent,  // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
  } = require('@openzeppelin/test-helpers');  


async function deploy(name, ...params) {
    const Contract = await ethers.getContractFactory(name);
    return await Contract.deploy(...params).then(f => f.deployed());
}



describe("DistributionAuction", () => {
    let AuctionContract;
    let ClubContract;

    before(async () => {
        [owner, addr1, addr2, addr3, _] = await ethers.getSigners();      

        //deploy the club contract
        ClubContract = await deploy(
            'FootballClub', 
            "http://localhost:3001/api",
            merkleRoot, 
            50000000
        )

        AuctionContract = await deploy(
            'DistributionAuction',
            ClubContract.address,
            12,
            100,
            100
        );

    });

    describe("context for testing", () => {
        it("Deploys smart contracts properly", async () => {
            assert(
                AuctionContract.address !== "" &&
                    ClubContract.address !== ""
            );
        });
    });
    /*
    describe("dutchAuction", () => {
        context("there are clubs left", () => {
            context("the owner sends the contract call", async () => {
                it("adds to the array of historical auctions", async () => {
                    await auctionContract.dutchAuction({ from: accounts[0] });

                    const auctions = await auctionContract.numAuctions();

                    expect(auctions.toNumber()).to.equal(1);
                });
            });

            context("a random address sends the contract call", async () => {
                it("reverts", async () => {
                    truffleAssert.reverts(
                        auctionContract.dutchAuction({ from: accounts[2] })
                    );
                });
            });
        });

        context("there are no clubs left", () => {
            before(async () => {
                await auctionContract.noClubs({ from: accounts[0] });
            });

            after(async () => {
                await auctionContract.addClubs({ from: accounts[0] });
            });

            it("reverts the transaction", async () => {
                truffleAssert.reverts(
                    auctionContract.dutchAuction({ from: accounts[0] })
                );
            });
        });
    });

    describe("currentPrice", () => {
        context("60 seconds has passed", () => {
            it("is roughly 10% less", async () => {
                const price = await auctionContract.currentPrice();

                await timeHelper.timeTravel(60);

                const newPrice = await auctionContract.currentPrice();

                expect(newPrice.toNumber()).to.be.below(
                    price.toNumber() * 0.91
                );
            });
        });

        context("a club has been sold", () => {
            before(async () => {
                await auctionContract.addClubSold({ from: accounts[0] });

                await auctionContract.dutchAuction({ from: accounts[0] });
            });

            after(async () => {
                await auctionContract.removeClubSold({ from: accounts[0] });
            });

            it("has a higher starting price", async () => {
                const newPrice = await auctionContract.currentPrice();

                expect(newPrice.toNumber()).to.be.above(19000);
            });
        });
    });

    describe("bid", () => {
        context("there is no running auction", () => {
            it("reverts the transaction", async () => {
                truffleAssert.reverts(
                    auctionContract.bid(3, { from: accounts[0], value: 1000 })
                );
            });
        });

        context("there is a running auction", () => {
            context("The bidder fails to send enough eth", () => {
                before(async () => {
                    await auctionContract.dutchAuction({ from: accounts[0] });
                });

                it("reverts the transaction", async () => {
                    truffleAssert.reverts(
                        auctionContract.bid(1, {
                            from: accounts[1],
                            value: 1000,
                        })
                    );
                });
            });
        });

        context("there is a running auction", () => {
            context("the bidder sents enough eth", () => {
                before(async () => {
                    await auctionContract.dutchAuction({ from: accounts[0] });
                });

                it("doesn't revert", async () => {
                    await auctionContract.bid(1, {
                        from: accounts[1],
                        value: 12000,
                    });
                });

                it("adds to the number of clubs sold", async () => {
                    const numSold = await auctionContract.getNumClubsSold();

                    expect(numSold.toNumber()).to.equal(1);
                });

                it("reduces the number of clubs left", async () => {
                    const numLeft = await auctionContract.clubsLeft();

                    expect(numLeft.toNumber()).to.equal(19);
                });

                it("successfully transfers the club to the bidder", async () => {
                    const owner = await clubContract.ownerOf(1);

                    expect(owner).to.equal(accounts[1]);
                });

                it("triggers a new dutchAuction", async () => {
                    const auctions = await auctionContract.numAuctions();

                    expect(auctions.toNumber()).to.equal(5);
                });
            });
        });

        context("a successful bid has triggered another auction", () => {
            context("the bidder tries to mint the same token", () => {
                it("reverts the transaction", async () => {
                    truffleAssert.reverts(
                        auctionContract.bid(1, {
                            from: accounts[1],
                            value: 20000,
                        })
                    );
                });
            });

            context("the bidder tries to mint a different token", () => {
                it("is successful", async () => {
                    await auctionContract.bid(2, {
                        from: accounts[1],
                        value: 20000,
                    });
                });
            });
        });
    });

    describe("withdrawBalance", () => {
        context("there is money in the contract", () => {
            //            let balance1 = await web3.eth.getBalance(accounts[1]);

            it("withdraws the money to the contract owner", async () => {
                await auctionContract.withdrawBalance({ from: accounts[0] });
            });
        });
    });*/
});
