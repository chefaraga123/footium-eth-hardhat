require('@nomiclabs/hardhat-waffle');
require('@nomiclabs/hardhat-ethers');

const settings = {
  optimizer: {
    enabled: true,
    runs: 200,
  },
};
const TEST_PRIVATE_KEY = "85b0f7cf636d1b79431c967657a3dabb9e9704b00fd52a63e82af030cb3b4dc4"
/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: {
    compilers: [
      { version: '0.8.4',  settings },
      { version: '0.7.6',  settings },
      { version: '0.6.12', settings },
      { version: '0.5.16', settings },
    ],
  },
  gasReporter: {
    currency: 'USD',
    coinmarketcap: process.env.COINMARKETCAP,
    gasPrice: 200,
  },
  networks: {
    rinkeby: {
      url: "https://rinkeby.infura.io/v3/ecfb8435109d43d1911477f67d002c77",
      accounts: [`0x${TEST_PRIVATE_KEY}`],
    },
  }
};
