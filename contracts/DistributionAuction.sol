// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./FootballClub.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

/**
 - auctions are discrete events, with divisions being auctioned one at a time
 - a league has to be generated on the game-engine for them to be then auctioned off 

  - bidding proccess
  - process of transferring the asset to the buyer 

  - once instantiated, the auctioning process is continuous accross the entire league 
  - one can know the characteristics of the auction by 
     - 
 */
contract DistributionAuction is Ownable {
    using SafeMath for uint256;

    struct Auction {
        uint256 numClubs;
        uint256 startTime;
        uint256 endTime;
        uint256 startPrice;
    }

    struct BondingCurve {
        uint256 numClubsSold;
        uint256 initialStartingPrice;
        uint256 marginalIncrease;
    }

    /* state variables */

    address public clubAddress; //Ethereum address of the club contract

    uint256 public leagueSize;
    uint256 public clubsLeft;
    uint256 public numAuctions;

    BondingCurve public currentBondingCurve;
    Auction public currentAuction;
    Auction[] public pastAuctions; //array of historical auctions

    FootballClub public footiumClubContract;

    /* events */

    event ClubSold(address indexed buyer, uint256 price);

    event NewAuction(uint256 startingPrice, uint256 numClubs);

    /* access control */

    modifier clubs() {
        require(clubsLeft != 0);
        _;
    }

    constructor(
        address _clubAddress,
        uint256 _leagueSize,
        uint256 _initialStartingPrice,
        uint256 _marginalIncrease
    ) {
        clubAddress = _clubAddress;
        leagueSize = _leagueSize;
        clubsLeft = _leagueSize;
        currentBondingCurve = BondingCurve(
            0,
            _initialStartingPrice,
            _marginalIncrease
        );

        footiumClubContract = FootballClub(_clubAddress);
    }

    /* external functions */

    function getNumClubsSold() external view returns (uint256) {
        return currentBondingCurve.numClubsSold;
    }

    function noClubs() external onlyOwner {
        clubsLeft = 0;
    }

    function addClubs() external onlyOwner {
        clubsLeft = 20;
    }

    function addClubSold() external onlyOwner {
        currentBondingCurve.numClubsSold += 1;
    }

    function removeClubSold() external onlyOwner {
        currentBondingCurve.numClubsSold -= 1;
    }

    function endAuction() public onlyOwner {
        currentAuction.startPrice = 0;
        currentAuction.startTime = 0;
        currentAuction.endTime = 0;
        currentAuction.numClubs = 0;
    }

    /* public functions */

    function dutchAuction() external clubs {
        if (msg.sender != owner() && msg.sender != address(this)) {
            revert();
        }

        uint256 _movement =
            currentBondingCurve.marginalIncrease *
                currentBondingCurve.numClubsSold;

        uint256 _startingPrice =
            currentBondingCurve.initialStartingPrice + _movement;

        currentAuction = Auction(
            clubsLeft,
            block.timestamp,
            block.timestamp + 600,
            _startingPrice
        );

        numAuctions += 1;

        pastAuctions.push(currentAuction);

        emit NewAuction(_startingPrice, clubsLeft);
    }

    function bid(uint256 _clubId) public payable clubs {
        uint256 price = currentPrice();

        require(msg.value >= price, "must transfer enough eth");

        footiumClubContract.takeOwnership(_clubId, msg.sender);

        currentBondingCurve.numClubsSold += 1;

        clubsLeft -= 1;

        payable(msg.sender).transfer(msg.value - price);

        //emit the clubSold event
        emit ClubSold(msg.sender, msg.value);

        //triggers a new auction
        if (clubsLeft > 0) {
            this.dutchAuction();
        }
    }

    function currentPrice() public view returns (uint256) {
        uint256 timePassed = block.timestamp - currentAuction.startTime;

        uint256 totalTime = currentAuction.endTime - currentAuction.startTime;

        uint256 value =
            currentAuction.startPrice -
                (currentAuction.startPrice * timePassed) /
                totalTime;

        if (value > 0) {
            return value;
        }
    }

    function withdrawBalance() external onlyOwner {
        payable(msg.sender).transfer(address(this).balance);
    }
}
