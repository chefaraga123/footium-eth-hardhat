// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

import "./FootballClub.sol";
import "./PlayerStaking.sol";

import "hardhat/console.sol";

contract FootballPlayer is ERC721, Ownable {
    using SafeMath for uint256;

    uint256 public constant playerLowerAgeLimit = 17;
    uint256 public constant academyAgeLimit = 21;

    uint256 public tokenId; 

    FootballClub public footballClub; // instantiates a copy of a footballclub to be interacted with

    struct Player {
        uint256 academyClubIndex; // the club id, the club the player was initially assigned to
        uint256 birthYr; //Birth Year, Age = currentyear - birth year
        uint256 generationId; // generationId what uniquely identifies it for that birthYr of player
    }

    struct PlayerBatch {
        uint256 academyClubIndex;
        uint256 birthYr;
        uint256 generationId;
        uint256 clubTo;
    }

    // the attributes of the bid which is being made for a player
    struct playerBid {
        uint256 bidder;
        uint256 bidValue;
        uint256 playerId;
    }

    // the attributes of the arrangements surrounding the sale of a player
    struct playerTender {
        uint256 minValue; // the minimum value the club will accept for the player they've put up for sale
        bool isForSale;
    }

    // index in array corresponds to Player similar to how player Id does
    Player[] public players;
    // list of player bids
    playerBid[] public clubsBids;

    address public stakingAddress;
    string private baseURI;

    mapping(uint256 => Player) public playerCheck;
    mapping(uint256 => uint256) public playerToClub;
    mapping(uint256 => uint256) public clubPlayerCount;
    mapping(uint256 => bool) public playerMintCheck;
    mapping(uint256 => playerTender) public playerTendered;
    mapping(address => uint256) public deposits;


    // player staking functionality
    mapping(uint256 => address) public playerToAddress;

    // academic year  => generationId => birth year
    mapping(uint256 => mapping(uint256 => mapping(uint256 => bool)))
        private playerAttributes;

    // for players in waiting to be accepted by club owner or not
    mapping(uint256 => uint256[]) joinClubRequests;

    // for pending in queue players requesting entry
    mapping(uint256 => bool) entryRequested;

    // player composite IDs
    mapping(uint256 => string) compositeIds;

    //Player Transferring
    event PlayerTransfer(
        uint256 indexed playerId,
        uint256 indexed clubFrom,
        uint256 indexed clubTo
    );

    //Player Minting
    event PlayerMinted(
        uint256 indexed playerId,
        uint256 indexed clubId,
        uint256 birthYear,
        uint256 generationId
    );

    //Player Trading
    event PlayerPutUpForSale(
        uint256 indexed playerId, 
        uint256 minValue
    );

    event PlayerOfferWithdrawn(uint256 indexed playerId);
    event PlayerBought(
        uint256 indexed playerId,
        uint256 bidValue,
        uint256 indexed clubFrom,
        uint256 indexed clubTo
    );

    event ClubWithdrawn(address indexed addr);
    event PlayerStaked(uint256 indexed playerId);
    event PlayerAvailableForLoan(
        uint256 indexed playerId,
        uint256 indexed lenderClubId,
        uint256 fee
    );
    event PlayerBorrowed(
        uint256 indexed playerId,
        uint256 indexed borrowerClubId,
        uint256 fee
    );
    event SetManager(
        uint256 indexed playerId, 
        uint256 indexed clubId
    );

    //EIP2309 standard
    event ConsecutiveTransfer(
        uint256 indexed fromTokenId,
        uint256 toTokenId,
        address indexed fromAddress,
        address indexed toAddress
    );

    constructor(address _footballClubAddress, string memory baseTokenURI)
        
        ERC721("Footium Player", "FPR")
    {
        footballClub = FootballClub(_footballClubAddress);
        baseURI = baseTokenURI;
    }


    function checkPlayerSaleStatus(uint256 _playerId) external view returns(bool) {
        playerTender memory saleStatus = playerTendered[_playerId];

        return saleStatus.isForSale;
    }


    function checkPlayerSalePrice(uint256 _playerId) external view returns(uint256) {
        playerTender memory price = playerTendered[_playerId];

        return price.minValue;
    }


    function setStakingAddress(address _stakingAddress) public onlyOwner {
        stakingAddress = _stakingAddress;
    }


    function mapPlayerToClub(uint256 playerId, uint256 clubId) internal {
        // maps the player to the owner club
        playerToClub[playerId] = clubId;

        //updates the number of players belonging to the given club
        clubPlayerCount[clubId] = SafeMath.add(clubPlayerCount[clubId], 1);
    }

    function addMintedPlayerToClub(
        uint256 _academyClubIndex,
        uint256 _birthYr,
        uint256 _generationId,
        uint256 clubId
    ) public {
        // adds a new player to the player array
        players.push(Player(_academyClubIndex, _birthYr, _generationId));

        // add player to map for future reference
        playerAttributes[_academyClubIndex][_generationId][_birthYr] = true;

        //player id corresponds to their position in a list of created players
        uint256 playerId = players.length;

        //adds the playerId to a list of minted player Ids
        playerMintCheck[playerId] = true;

        // minting erc721 token
        console.log(tokenId+1);

        _mint(msg.sender, tokenId+1);

        tokenId++;

        // checks that the player hasn't been assigned yet
        assert(playerToClub[playerId] == 0);

        mapPlayerToClub(playerId, clubId);

        playerToAddress[playerId] = msg.sender;

        // adds a new composite ID
        string memory compositeId =
            string(
                abi.encodePacked(
                    _birthYr,
                    "-",
                    _academyClubIndex,
                    "-",
                    _generationId
                )
            );
        compositeIds[playerId] = compositeId;

        emit PlayerMinted(playerId, _academyClubIndex, _birthYr, _generationId);
    }

    function mintPlayer(
        uint256 _academyClubIndex,
        uint256 _birthYr,
        uint256 _generationId,
        uint256 clubTo
    ) public {
        require(
            !playerAttributes[_academyClubIndex][_generationId][_birthYr],
            "Player already exists"
        );

        // TODO: THIS NEEDS FIXING, CURRENT YEAR CANNOT BE FIXED
        uint256 playerAge = (block.timestamp / (365 days)) + 1970 - _birthYr;

        require(
            msg.sender == footballClub.ownerOf(clubTo),
            "You do not own this football club"
        );

        require(
            playerAge >= playerLowerAgeLimit, 
            "player is of age"
        );

        if (academyAgeLimit >= playerAge && playerAge >= playerLowerAgeLimit) {
            require(
                _academyClubIndex == clubTo, 
                "mints to academy owner"
            );
        }

        addMintedPlayerToClub(
            _academyClubIndex,
            _birthYr,
            _generationId,
            clubTo
        );
    }

    function mintBatchPlayers(PlayerBatch[] memory _players) public {
        require(_players.length <= 11, "Too many players");

        uint256 i;

        int256 tokenidFrom = -1;
        int256 tokenidTo = -1;

        for (i = 0; i < _players.length; i++) {
            uint256 playerAge =
                (block.timestamp / 365 days) - _players[i].birthYr;

            if (msg.sender != footballClub.ownerOf(_players[i].clubTo)) {
                continue;
            }
            if (playerAge < playerLowerAgeLimit) {
                continue;
            }

            if (
                playerAttributes[_players[i].academyClubIndex][
                    _players[i].generationId
                ][_players[i].birthYr]
            ) {
                continue;
            }

            addMintedPlayerToClub(
                _players[i].academyClubIndex,
                _players[i].birthYr,
                _players[i].generationId,
                _players[i].clubTo
            );
        }

        emit ConsecutiveTransfer(
            uint256(tokenidFrom),
            uint256(tokenidTo),
            address(this),
            msg.sender
        );
    }

    // for erc721 standards in which a non club owner buys the player
    function mintAsIndividual(
        uint256 _academyClubIndex,
        uint256 _birthYr,
        uint256 _generationId
    ) public {
        uint256 playerAge = 2020 - _birthYr;

        require(
            playerAge >= academyAgeLimit, 
            "must be eligible to leave the academy"
        );

        require(
            !playerAttributes[_academyClubIndex][_generationId][_birthYr],
            "Player already exists"
        );

        players.push(Player(_academyClubIndex, _birthYr, _generationId));

        // add player to map for future reference
        playerAttributes[_academyClubIndex][_generationId][_birthYr] = true;

        //adds the playerId to a list of minted player Ids
        playerMintCheck[tokenId+1] = true;

        _mint(msg.sender, tokenId+1);
    }

    function requestPlayerEntryToClub(uint256 clubTo, uint256 playerId) public {
        require(entryRequested[playerId] == false, "Already in queue");
        require(
            ownerOf(playerId) == msg.sender,
            "you dont own this player to make the required request"
        );

        entryRequested[playerId] = true;
        joinClubRequests[clubTo].push(playerId);
    }

    function approvalFromClub(
        uint256 clubToId,
        uint256 playerId,
        bool result
    ) public {
        require(
            msg.sender == footballClub.ownerOf(clubToId),
            "You do not own this football club"
        );

        // remove the entry since the player need to apply in some other club if not accepted
        entryRequested[playerId] = false;

        // if accepted add to club
        if (result == true) {
            mapPlayerToClub(playerId, clubToId);
        } else {
            delete joinClubRequests[clubToId][playerId];
        }
    }

    function changeClubMapping(uint256 playerId, uint256 clubTo) public {
        require(
            ownerOf(playerId) == msg.sender,
            "you dont own this player to make the required request"
        );
        delete joinClubRequests[clubTo][playerId];
        entryRequested[playerId] = true;
        joinClubRequests[clubTo].push(playerId);
    }

    function getSignedPlayersByClub(uint256 _clubIndex)
        public
        view
        returns (uint256[] memory)
    {
        uint256[] memory result = new uint256[](clubPlayerCount[_clubIndex]);
        uint256 counter = 0;

        // goes through a list of all the playerIds, checking each one if they are belong to the given club

        for (uint256 i = 0; i < players.length; i += 1) {
            if (playerToClub[i] == _clubIndex) {
                result[counter] = i;
                counter += 1;
            }
        }

        return result;
    }

    function _transfer(
        uint256 _clubFrom,
        uint256 _clubTo,
        uint256 _playerId
    ) internal {
        require(_exists(_playerId), "Player does not exist.");
        require(_clubFrom != _clubTo, "Cannot transfer to the same club.");
        require(
            playerToClub[_playerId] == _clubFrom,
            "Club does not own the player."
        );

        clubPlayerCount[_clubTo] = SafeMath.add(clubPlayerCount[_clubTo], 1);
        clubPlayerCount[_clubFrom] = SafeMath.sub(
            clubPlayerCount[_clubFrom],
            1
        );
        playerToClub[_playerId] = _clubTo;
    }

    /* When players are being staked, they remain bound to an underlying club 
    // value is conferred on the staking club 
    // However it needs to be sent to the staking contract - which is not a club
    // Therefore there needs to be a facility for sending the NFT to an address */
    function transferForStaking(
        address _from,
        address _to,
        uint256 _playerId
    ) public {
        require(_from != address(0) && _to != address(0), "Invalid addresses");

        require(_exists(_playerId), "Player does not exist.");
        
        require(_from != _to, "Cannot transfer to the same address.");
        require(
            playerToAddress[_playerId] == _from,
            "Club does not own the player."
        );

        playerToAddress[_playerId] = _to;

        //read by the front end to stop attempted transfer of the player once staked
        emit PlayerStaked(_playerId);
    }

    function balanceOfInternal(uint256 _clubIndex)
        public
        view
        returns (uint256 _balance)
    {
        return clubPlayerCount[_clubIndex];
    }

    function ownerOfUint(uint256 _playerId)
        public
        view
        returns (uint256 _clubIndex)
    {
        uint256 _owner = playerToClub[_playerId];

        return _owner;
    }

    function _exists(uint256 _playerId) internal view override returns (bool) {
        uint256 _owner = playerToClub[_playerId];

        return _owner != 0;
    }

    function exists(uint256 _playerId) public view returns (bool) {
        uint256 _owner = playerToClub[_playerId];

        return _owner != 0;
    }

    
    function testOwner(uint256 _playerId) public view returns (uint256) {
        uint256 _owner = playerToClub[_playerId];

        return _owner;
    }


    function isContract(address account) internal view returns (bool) {
        uint256 size;

        assembly {
            size := extcodesize(account)
        }

        return size > 0;
    }

    function totalSupply() public view returns (uint256) {
        return uint256(players.length);
    }

    function offerPlayerForSale(uint256 _playerId, uint256 _clubId, uint256 _minVal) public {
        // ascertain which club currently owns the player
        
        require(
            playerToClub[_playerId] == _clubId,
            "Player auctioned by club that owns it"
        );
        
        console.log('owner',footballClub.ownerOf(_clubId));
        console.log('transaction sender',msg.sender);

        require(
            footballClub.ownerOf(_clubId) == msg.sender,
            "transaction sender owns the club"
        );

        // creates a mapping between a player & the nature of the sale
        playerTendered[_playerId] = playerTender(_minVal, true);

        emit PlayerPutUpForSale(_playerId, _minVal);
    }

    // buying and selling exist independently
    function buyPlayer(uint256 _playerId, uint256 _clubFrom) public payable {
        require(
            msg.sender == footballClub.ownerOf(_clubFrom),
            "Buyer is the sender"
        );
        require(
            _clubFrom != playerToClub[_playerId],
            "Club buying the player cannot already own the player"
        );
        require(
            msg.value >= playerTendered[_playerId].minValue,
            "The offered price must be equalled or exceeded"
        );

        uint256 playerSeller = playerToClub[_playerId];
        uint256 playerBuyer = _clubFrom;

        address _playerSeller = footballClub.ownerOf(playerToClub[_playerId]);

        //checking if the player is currently staked
        if(stakingAddress != address(0)) {
            PlayerStaking stakingContract = PlayerStaking(stakingAddress);

            bool stakeStatus = stakingContract.playerStakeStatus(_playerId);

            if (stakeStatus == true) {
                //returns the staked player to the selling club, prior to the transfer
                stakingContract.withdrawPlayer(_playerId);
            }
        }



        deposits[_playerSeller] += msg.value;

        _transfer(playerSeller, playerBuyer, _playerId);

        playerTendered[_playerId].isForSale = false;

        emit PlayerBought(_playerId, msg.value, playerSeller, playerBuyer);
    }

    function withdrawPlayerFromSale(uint256 _playerId, uint256 _clubId)
        public
        payable
    {
        require(
            msg.sender == footballClub.ownerOf(_clubId),
            "sender owns the club"
        );

        playerTendered[_playerId].isForSale = false;

        emit PlayerOfferWithdrawn(_playerId);
    }

    function offerPlayerForLoan(
        uint256 _playerId,
        uint256 _lenderClubId,
        uint256 _fee
    ) public {
        require(
            msg.sender == footballClub.ownerOf(_lenderClubId),
            "sender owns the club"
        );

        emit PlayerAvailableForLoan(_playerId, _lenderClubId, _fee);
    }

    function borrowPlayer(
        uint256 _playerId,
        uint256 _borrowerClubId,
        uint256 _fee
    ) public payable {
        require(
            msg.sender == footballClub.ownerOf(_borrowerClubId),
            "sender owns the club"
        );

        emit PlayerBorrowed(_playerId, _borrowerClubId, _fee);
    }

    function setManager(uint256 _playerId, uint256 _clubId) public {
        require(
            msg.sender == footballClub.ownerOf(_clubId),
            "sender owns the club"
        );

        emit SetManager(_playerId, _clubId);
    }

    function withdraw() public {
        uint256 amount = deposits[msg.sender];

        deposits[msg.sender] = 0;

        payable(msg.sender).transfer(amount);

        emit ClubWithdrawn(msg.sender);
    }

    function tokenURI(uint256 _tokenId)
        public
        view
        virtual
        override
        returns (string memory)
    {   

        require(
            _exists(_tokenId),
            "ERC721Metadata: URI query for nonexistent token"
        );

        string memory compositeId = compositeIds[_tokenId];
        console.log('compositeId:',compositeId);

        return
            bytes(baseURI).length > 0
                ? string(abi.encodePacked(baseURI, compositeId, "/metadata"))
                : "";
    }
}
