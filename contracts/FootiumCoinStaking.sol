// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "./FootiumCoin.sol";
import "hardhat/console.sol";

contract FootiumCoinStaking {
    using SafeMath for uint256;

    /* - State Variables - */

    FootiumCoin footiumCoinContract;

    address public tokenAddress;
    uint256 public totalStaked;
    uint256 public minStakeDuration;
    uint256 public emergencyWithdrawFee;

    mapping(uint256 => uint256) public clubBalances;
    mapping(uint256 => uint256) public depositTime;
    mapping(uint256 => bool) public clubStakeStatus;

    /* - Events - */
    event tokensStaked(uint256 indexed staker, uint256 amount);
    event tokensWithdrawn(uint256 indexed staker, uint256 amount);

    constructor(
        address _tokenAddress,
        uint256 _minStakeDuration,
        uint256 _emergencyWithdrawFee
    ) {
        tokenAddress = _tokenAddress;
        footiumCoinContract = FootiumCoin(_tokenAddress);
        minStakeDuration = _minStakeDuration;
        emergencyWithdrawFee = _emergencyWithdrawFee;
    }

    /* - View Functions - */
    function getInvestmentExpiry(uint256 _clubId)
        external
        view
        returns (uint256)
    {
        return depositTime[_clubId] + minStakeDuration;
    }

    function getCurrentTime() external view returns (uint256) {
        return block.timestamp;
    }

    /* - Club Staking Functionality - */

    function createStake(uint256 _amount, uint256 _clubId) public payable {
        require(_amount > 0, "You need to stake more than zero tokens");

        uint256 allowance =
            footiumCoinContract.allowance(msg.sender, address(this));

        require(allowance >= _amount, "Check the token allowance");

        clubStakeStatus[_clubId] = true;

        depositTime[_clubId] = block.timestamp;

        footiumCoinContract.transferFrom(msg.sender, address(this), _amount);

        //associates a club with a balance - accounts can have multiple clubs
        clubBalances[_clubId] += _amount;

        emit tokensStaked(_clubId, _amount);
    }

    function withdrawStake(
        uint256 _amount,
        uint256 _clubId,
        address staker
    ) public {
        require(_amount > 0, "Can't withdraw nothing");
        require(
            clubBalances[_clubId] != 0,
            "club must have a balance to withdraw"
        );

        clubBalances[_clubId] -= _amount;

        uint256 remainingAmount;

        //checks if the stake is being withdrawn prematurely
        if (block.timestamp < depositTime[_clubId] + minStakeDuration) {
            uint256 fee = (emergencyWithdrawFee * _amount) / 100;
            remainingAmount = _amount - fee;
        } else {
            remainingAmount = _amount;
        }

        footiumCoinContract.transfer(staker, remainingAmount);

        //indicates that the entire stake has been withdrawn
        if (clubBalances[_clubId] == 0) {
            clubStakeStatus[_clubId] = false;
        }

        emit tokensWithdrawn(_clubId, _amount);
    }
}
