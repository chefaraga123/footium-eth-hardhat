// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

import "./FootiumCoinStaking.sol";

contract FootballClub is ERC721Enumerable, Ownable {
    using SafeMath for uint256;
    using Address for address;

    using Strings for uint256;
    

    address public stakingAddress;
    address public auctionAddress;
   
    uint256 public tokenId; 
    uint256 public price;

    uint256 public constant preMintLimit = 220;
    uint256 public preMints;

    uint256[] public tokenIdArray; 


    string private baseURI;

    bytes32 immutable public merkleroot;



    constructor(
        string memory baseTokenURI, 
        bytes32 _merkleroot, 
        uint256 _price
    ) ERC721("Footium Club", "FCB") 
    {
        merkleroot = _merkleroot;
        baseURI = baseTokenURI;
        price = _price;
    }

    function preMint(address _account) external onlyOwner 
    {
        require(preMints <= 220, "only mint 220 to advisors");

        tokenId++;        
        _safeMint(_account, tokenId);

    }

    function redeem(address account, bytes32[] calldata proof) external payable
    {   
        require(msg.value >= price * 1 gwei, "insufficient payment");
        require(_verify(_leaf(account), proof), "Invalid merkle proof");
        
        _safeMint(account, tokenId+1);
        tokenId++;

    }

    function _leaf(address account) internal pure returns (bytes32)
    {
        return keccak256(abi.encodePacked(account));
    }

    function _verify(bytes32 leaf, bytes32[] memory proof) internal view returns (bool)
    {
        return MerkleProof.verify(proof, merkleroot, leaf);
    }

    function tokenURI(uint256 _tokenId) public view virtual override returns (string memory)
    {
        require(
            _exists(_tokenId),
            "URI query for nonexistent token"
        );

        return
            bytes(baseURI).length > 0
                ? string(abi.encodePacked(baseURI, _tokenId, "/metadata"))
                : "";
    }


    struct ClubOffer {
        bool isForSale;
        uint256 minValue;
    }

    struct ClubBid {
        bool hasBid;
        address bidder;
        uint256 value;
        uint256 clubIndex;
    }

    ClubBid[] public accountBids;

    FootiumCoinStaking private stakingContract;

    mapping(uint256 => ClubOffer) public clubsOfferedForSale;
    mapping(uint256 => ClubBid) public clubsHighestBid;
    mapping(address => uint256) public pendingWithdrawals;
    mapping(address => ClubBid[]) public bidsByAddress;

    event ClubOffered(
        uint256 indexed clubId, 
        uint256 minValue
    );
    event ClubBought(
        uint256 indexed clubId,
        uint256 value,
        address indexed fromAddress,
        address indexed toAddress
    );
    event ClubBidPlaced(
        uint256 indexed clubId,
        uint256 value,
        address indexed fromAddress
    );
    event ClubBidAccepted(
        uint256 indexed clubId,
        uint256 value,
        address indexed fromAddress,
        address indexed toAddress
    );
    event ClubNoLongerForSale(uint256 indexed clubId);
    event ClubBidWithdrawn(uint256 indexed clubId);
    event Withdrawn(address indexed addr);
    event ClubTransfer(address from, address to, uint256 tokenId);


    function setStakingAddress(address _stakingAddress) public onlyOwner {
        require(_stakingAddress != address(0), "bad staking address");

        stakingAddress = _stakingAddress;
    }


    function setAuctionAddress(address _auctionAddress) public onlyOwner {
        require(_auctionAddress != address(0), "bad auction address");

        auctionAddress = _auctionAddress;
    }


    function takeOwnership(uint256 _number, address _buyer) public {
        uint256 _tokenId = uint256(_number);

        if (msg.sender != owner() && msg.sender != auctionAddress) {
            revert("must be sold via the auction");
        }

        _safeMint(_buyer, _tokenId);
    }
    

    function offerClubForSale(uint256 clubIndex, uint256 minSalePriceInWei)
        public
    {
        require(msg.sender == this.ownerOf(clubIndex), "must be owner");

        clubsOfferedForSale[clubIndex] = ClubOffer(true, minSalePriceInWei);

        emit ClubOffered(clubIndex, minSalePriceInWei);
    }

    function placeBid(uint256 clubIndex) public payable {
        require(msg.sender != this.ownerOf(clubIndex), "cannot bid on own club");
        require(msg.value > 0, "bid must be non-zero");

        ClubBid memory highestBid = clubsHighestBid[clubIndex];

        require(msg.value > highestBid.value, "bids must be increasing");

        pendingWithdrawals[highestBid.bidder] += highestBid.value;
        
        clubsHighestBid[clubIndex] = ClubBid(
            true,
            msg.sender,
            msg.value,
            clubIndex
        );

        emit ClubBidPlaced(clubIndex, msg.value, msg.sender);
    }

    function acceptBid(uint256 _clubIndex) public {
        require(msg.sender == ownerOf(_clubIndex), 
        "only owner can accept bids"
        );

        stakingContract = FootiumCoinStaking(stakingAddress);

        address seller = ownerOf(_clubIndex);

        //check if the club has an associated stake
        if (stakingContract.clubStakeStatus(_clubIndex) == true) {
            //withdraw the stake and return it to the current/pre-transfer owner
            uint256 _amount = stakingContract.clubBalances(_clubIndex);

            //withdraws the total stake to the seller.
            stakingContract.withdrawStake(_amount, _clubIndex, seller);
        }

        ClubBid memory highestBid = clubsHighestBid[_clubIndex];

        pendingWithdrawals[msg.sender] += highestBid.value;

        // reset the bidding/buying state
        clubsHighestBid[_clubIndex] = ClubBid(false, address(0), 0, _clubIndex);
        clubsOfferedForSale[_clubIndex] = ClubOffer(false, 0);

        safeTransferFrom(msg.sender, highestBid.bidder, _clubIndex);

        emit ClubBidAccepted(
            _clubIndex,
            highestBid.value,
            msg.sender,
            highestBid.bidder
        );
    }

    function buyClub(uint256 _clubId) public payable {

        require(
            msg.sender != ownerOf(_clubId), 
            "Cannot buy own the club"    
        );

        ClubOffer memory clubOffer = clubsOfferedForSale[_clubId];

        require(clubOffer.isForSale, "Club must be for sale");

        // fails if the buying price is smaller than the offer
        require(msg.value >= clubOffer.minValue, "insufficient funds");

        address seller = ownerOf(_clubId);

        //instantiate a copy of the staking contract
        stakingContract = FootiumCoinStaking(stakingAddress);

        //check if the club has an associated stake
        bool stakeStatus = stakingContract.clubStakeStatus(_clubId);

        if (stakeStatus == true) {
            //withdraw the stake and return it to the current/pre-transfer owner
            uint256 _amount = stakingContract.clubBalances(_clubId);

            //withdraws the total stake to the seller.
            stakingContract.withdrawStake(_amount, _clubId, seller);
        }

        // credits the seller's account with the sale price
        pendingWithdrawals[seller] += msg.value;

        // emits the club bought event
        emit ClubBought(_clubId, msg.value, seller, msg.sender);

        // reset the bidding/buying state
        ClubBid memory highestBid = clubsHighestBid[_clubId];

        if (highestBid.hasBid) {
            pendingWithdrawals[highestBid.bidder] += highestBid.value;
        }

        clubsHighestBid[_clubId] = ClubBid(false, address(0), 0, _clubId);
        clubsOfferedForSale[_clubId] = ClubOffer(false, 0);
        // TODO: refund the highest bidder

        safeTransferFrom(seller, msg.sender, _clubId);

        emit ClubTransfer(seller, msg.sender, _clubId);
        emit Transfer(seller, msg.sender, _clubId);
    }

    function withdrawBid(uint256 _clubId) public {
        address _address = msg.sender;

        ClubBid[] memory bidsList = bidsByAddress[_address];

        uint256 i;
        for (i = 0; i < bidsList.length; i++) {
            ClubBid memory _bid = bidsList[i];
            uint256 localClubIndex = _bid.clubIndex;
            if (_clubId == localClubIndex) {
                // correct bid found
                //uint _bidValue = _bid.value;
                _bid.hasBid = false;
            }
        }

        pendingWithdrawals[_address] = 0;

        emit ClubBidWithdrawn(_clubId);
    }

    function withdrawClubFromSale(uint256 _clubId) public {
        ClubOffer memory _clubOffer = clubsOfferedForSale[_clubId];

        _clubOffer.isForSale = false;

        emit ClubNoLongerForSale(_clubId);
    }

    function withdraw() public {
        uint256 amount = pendingWithdrawals[msg.sender];

        pendingWithdrawals[msg.sender] = 0;

        payable(msg.sender).transfer(amount);

        emit Withdrawn(msg.sender);
    }
}
