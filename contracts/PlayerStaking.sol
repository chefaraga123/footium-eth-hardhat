// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./FootballPlayer.sol";
import "./FootballClub.sol";

/* Stake ERC721 NFTs - i.e. transfer them to this contract  
// the contract is viewed by the game engine 
// the game engine hence calculates the rewards. W
// This contract simply tracks staked NFTs for the front end
*/

contract PlayerStaking {
    FootballPlayer footballPlayer;
    FootballClub footballClub;

    //associates a club Id to an array of deposited playerIds
    mapping(uint256 => uint256[]) public depositedPlayers;
    mapping(uint256 => uint256) public playerToOwner;
    mapping(uint256 => bool) public playerStakeStatus;
    mapping(uint256 => uint256) public depositTime;

    /* - Events - */
    event playerStaked(uint256 indexed staker, uint256 playerId);
    event playerWithdrawn(uint256 indexed staker, uint256 playerId);

    constructor(
        address _PlayerContractAddress,
        address _ClubContractAddress
    ) {
        footballPlayer = FootballPlayer(_PlayerContractAddress);
        footballClub = FootballClub(_ClubContractAddress);
    }

    function getStakedPlayers(uint256 _clubId)
        public
        view
        returns (uint256[] memory)
    {
        return depositedPlayers[_clubId];
    }

    function stakePlayer(uint256 _playerId) public {
        uint256 clubId = footballPlayer.ownerOfUint(_playerId);

        uint256[] memory playersArray = getStakedPlayers(clubId);

        require(playersArray.length < 3);

        address ownerAddress = footballClub.ownerOf(clubId);

        require(msg.sender == ownerAddress);

        playerToOwner[_playerId] = clubId;

        depositTime[_playerId] = block.timestamp;

        footballPlayer.transferForStaking(msg.sender, address(this), _playerId);

        playerStakeStatus[_playerId] = true;

        emit playerStaked(clubId, _playerId);
    }

    function withdrawPlayer(uint256 _playerId) public {
        uint256 clubId = footballPlayer.playerToClub(_playerId);

        require(playerToOwner[_playerId] == clubId);

        footballPlayer.transferForStaking(address(this), msg.sender, _playerId);

        playerStakeStatus[_playerId] = false;

        emit playerWithdrawn(clubId, _playerId);
    }
}
