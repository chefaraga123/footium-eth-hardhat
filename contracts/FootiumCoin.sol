// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";


contract FootiumCoin is ERC20, Ownable {
    using SafeMath for uint256;

    ////// State Variables /////////
    address public stakingAddress;
    address public clubStakingAddress;
    address public academyStakingAddress;
    address public crowdsaleAddress;

    uint256 public initialSupply;
    uint256 public circulatingSupply;

    ///// Modifiers //////
    modifier onlyCrowdsale {
        require(msg.sender == crowdsaleAddress, "crowdsale access privilege");
        _;
    }

    constructor(uint256 _initialSupply) ERC20("FootiumToken", "Foot") {
        _mint(msg.sender, _initialSupply);
        initialSupply = _initialSupply;
    }

    ///////// Functions setting external contract permissions //////////
    function setCrowdsale(address _crowdsaleAddress) public onlyOwner {
        require(_crowdsaleAddress != address(0), "cannot be zero address");

        crowdsaleAddress = _crowdsaleAddress;
    }

    /////// External contract permissions and interactions /////////
    function buyTokens(address _receiver, uint256 _amount)
        public
        onlyCrowdsale
    {
        require(_receiver != address(0));

        require(_amount > 0, "must be non-zero");

        transfer(_receiver, _amount);
    }

    function burnTokens(address _account, uint256 _amount) public {
        require(_amount > 0, "cannot burn zero tokens");

        _burn(_account, _amount);
    }
}
