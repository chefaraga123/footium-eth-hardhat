const whitelist = require('./testWhitelist.json');

const { MerkleTree } = require('merkletreejs');
const keccak256 = require('keccak256');
const { ethers } = require('hardhat');



function hashToken(account) {
    return Buffer.from(ethers.utils.solidityKeccak256(['address'], [account]).slice(2), 'hex')
}
  

const leaves = whitelist.addresses.map(token => hashToken(token))

const merkleTree = new MerkleTree(leaves, keccak256, { sortPairs: true });

const merkleRoot = merkleTree.getHexRoot()

module.exports = {
    merkleTree,
    merkleRoot 
}
